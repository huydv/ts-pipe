import { sum, User, getUser } from '../index';

describe('SUM function', () => {
  test('number + number: Should return valid number', () => {
    expect(sum(1, 2)).toBe(3);
  });

  test('number + number: Should return valid number', () => {
    expect(sum(12222222222222222222222222, 2)).toBe(1.2222222222222223e+25);
  });

  test('number + NaN: Should return NaN', () => {
    expect(sum(1, NaN)).toBe(NaN);
  });
  test('NaN + NaN: Should return NaN', () => {
    expect(sum(NaN, NaN)).toBe(NaN);
  });
});

describe('getUser', () => {
  const id = '1988';
  const undefinedId = '';

  test('{getUser} Should have property {displayName}', () => {
    expect(getUser(id)).toHaveProperty('displayName');
  });

  test('{getUser} Should return User instance', () => {
    expect(getUser(id)).toBeInstanceOf(User);
  });

  test('{getUser} Should return undefined', () => {
    expect(getUser(undefinedId)).toBeUndefined();
  });

  test('{getUser} Should be defined', () => {
    expect(getUser(id)).toBeDefined();
  });
});