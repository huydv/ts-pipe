export class User {
  displayName: string;
  firstName: string;
  lastName: string;
  constructor(first: string, last: string) {
    this.firstName = first;
    this.lastName = last;
    this.displayName = `${first} ${last}`;
  }
  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
export function sayHello(name: string) {
  return `Hello ${name}`;
}

export const sum = (a: number, b: number) => a + b;


export function getUser(id: string): User | undefined {
  if (!id) return undefined;
  return new User('Doan', 'Huy');
}